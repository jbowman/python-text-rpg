import os, sys, json

basicSaveInfo = []

def readBasicSaveInfo():
    if len(os.listdir('./saved_games')) > 0:
        for filename in os.listdir('./saved_games'):
            with open('./saved_games/' + filename) as saveFile:
                data = saveFile.read()
                jdata = json.loads(data)
                print(jdata["name"])